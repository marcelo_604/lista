#include <stdio.h>
#include <stdlib.h>



typedef struct {
  int num;
  struct Lista *prox;
}Lista;

Lista *inicia(Lista *n);
void mostraLista(Lista *p);
int vazio(Lista *p);
Lista *inserirComeco(Lista *p, int n);

//=======================
int main() {
  Lista *p = NULL; inicia(p);
  mostraLista(p);
  p = inserirComeco(p, 7);
  mostraLista(p);
  p = inserirComeco(p, 3);
  mostraLista(p);

  free(p);
  return 0;
}

//*********************************************

Lista *inicia(Lista *n) {
  n = (Lista *)malloc(sizeof(Lista));
  n->num = 0;
  n->prox = NULL;
  return n;
}

Lista *inserirComeco(Lista *p, int n) {
  //Lista *aux = inicia(aux);
  Lista *aux;
  aux = (Lista *)malloc(sizeof(Lista));
  aux->num = n;
  aux->prox = p;
  p = aux;
  return p;
}

void mostraLista(Lista *p) {
  Lista *aux = p;
  int i;
  if(!vazio(aux)) {
    printf("\n\nLista de valores\n");
    while(aux->prox != NULL)
      printf("|%5d",aux->num);
    printf("|\n\n");
  }
}

int vazio(Lista *p) {
  if(p->prox == NULL) {
    printf("\n\nA lista esta vazia!!\n\n");
    return 1;
  }
  else
    return 0;
}











//
